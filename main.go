package main

import (
    "bufio"
    "encoding/json"
    "fmt"
    "io/ioutil"
    "net/http"
    "net/http/cookiejar"
    "os"
    "regexp"
    "strings"
    "strconv"
    "time"
)

var DOM = "https://letterboxd.com"
type Response struct {
    Result   string
    Messages []string
    Csrf     string
}

type Liked struct {
    Result bool
    Csrf   string
    Liked  bool
    Rating int
}

func help() {
    fmt.Println("lbxd - manage letterboxd from the cli")
    fmt.Println("  like <url> - (un)like a given movie")
    fmt.Println("  review <url> - review a given movie")
    fmt.Println("  search <string> - find films based on input string")
    os.Exit(0)
}

func need_env() {
    fmt.Println("Need Letterboxd™ username and password.")
    fmt.Println("Can be set through `boxu' and `boxp' variables respectively.")
    os.Exit(0)
}

func getCSRF(client *http.Client) (csrf string) {
    req, _ := http.NewRequest("GET", DOM, nil)
    resp, _ := client.Do(req)
    for _, c := range resp.Cookies() {
        if strings.Contains(c.Name, "csrf") {
            csrf = c.Value
        }
    }
    return csrf
}

func login(user string, pass string) (*http.Client, string) {
    var result Response
    cookieJar, _ := cookiejar.New(nil)
    client := &http.Client{Jar: cookieJar}
    csrf := getCSRF(client)

    addr := DOM + "/user/login.do"
    req, _ := http.NewRequest("POST", addr, nil)
    qry := req.URL.Query()
    qry.Add("username", user)
    qry.Add("password", pass)
    qry.Add("remember", "false")
    qry.Add("__csrf", csrf)
    req.URL.RawQuery = qry.Encode()

    resp, err := client.Do(req)
    if err != nil {
        fmt.Println("Error logging in")
        os.Exit(1)
    }
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    json.Unmarshal(body, &result)
    if result.Result != "success" {
        fmt.Println(result.Messages)
        fmt.Println("Try adjusting your environment credentials")
        os.Exit(0)
    }
    return client, csrf
}

func findLine(body string, regex string) (line []string) {
    re := regexp.MustCompile(regex)
    if line = re.FindAllString(body, -1); line == nil {
        fmt.Println("no movie found")
        os.Exit(1)
    }
    return line
}

func getId(addr string, client *http.Client) string {
    req, _ := http.NewRequest("GET", addr, nil)
    resp, err := client.Do(req)
    if err != nil {
        fmt.Println("Bad film url")
        os.Exit(1)
    }
    defer resp.Body.Close()
    body, _ := ioutil.ReadAll(resp.Body)
    films := findLine(string(body), `film:(\d+)`)
    return films[0]
}

func iLike(addr string, client *http.Client, csrf string) {
    var status Liked
    filmId := getId(addr, client)
    id := strings.ReplaceAll(filmId, ":", "Id=")

    check := DOM + "/s/check-film-like?" + id
    req, _ := http.NewRequest("GET", check, nil)
    resp, _ := client.Do(req)
    body, _ := ioutil.ReadAll(resp.Body)
    json.Unmarshal(body, &status)
    if !status.Result {
        fmt.Println("Unable to check media like status")
        os.Exit(0)
    }

    post := DOM + "/s/" + filmId + "/like/"
    req, _ = http.NewRequest("POST", post, nil)
    qry := req.URL.Query()
    if status.Liked {
        id = "You don't like this."
        qry.Add("liked", "false")
    } else {
        id = "Liked!"
        qry.Add("liked", "true")
    }
    qry.Add("__csrf", csrf)
    req.URL.RawQuery = qry.Encode()
    client.Do(req)
    fmt.Println(id)
}

func search(query string, client *http.Client) {
    q := strings.ReplaceAll(query, " ", "+")
    addr := DOM + "/search/films/" + q + "/?adult"
    req, _ := http.NewRequest("GET", addr, nil)
    resp, err := client.Do(req)
    if err != nil {
        fmt.Println("Bad search query")
        os.Exit(1)
    }
    defer resp.Body.Close()
    body, _ := ioutil.ReadAll(resp.Body)
    str := strings.ReplaceAll(string(body), `&amp;`, `&`) // <- annoying
    str = strings.ReplaceAll(str, `&#039;`, `'`)          // find alternative
    str = strings.ReplaceAll(str, `&quot;`, `"`)

    fmt.Println(`Top 3 Results`)
    url_re := `href\=\"\/film\/[[:alnum:]&-:.]*\/\">`
    title_re := `[\p{Latin}[:alnum:] -;'&]*<`
    years := findLine(str, `\/films\/year\/(\d+)`)
    tails := findLine(str, url_re + title_re)
    for i := 0; i < 3; i++ {
        t := tails[i]
        y := years[i]
        sep := strings.Index(t, ">")
        fmt.Printf("\nTitle: %s\n", t[sep+1:len(t)-1])
        fmt.Printf("Year: %s\n", y[len(y)-4:])
        fmt.Printf("Url: %s\n", DOM + t[6:sep-1])
    }
}

func review(addr string, client *http.Client, csrf string) {
    var date, review, score string
    input := bufio.NewReader(os.Stdin)
    filmId := getId(addr, client)
    diary  := DOM + "/s/save-diary-entry"
    dex    := strings.Index(filmId, ":") + 1

    req, _ := http.NewRequest("POST", diary, nil)
    qry := req.URL.Query()
    qry.Add("__csrf", csrf)
    qry.Add("viewingId", "")
    qry.Add("filmId", filmId[dex:])
    fmt.Printf("(optional) Enter viewing date [yyyy-mm-dd]: ")
    date, _ = input.ReadString('\n')
    if date == "\n" {
        t := time.Now()
        date = fmt.Sprintf("%d-%02d-%02d", t.Year(), t.Month(), t.Day())
        qry.Add("viewingDateStr", date)
    } else {
        qry.Add("specifiedDate", "true")
        qry.Add("viewingDateStr", date)
    }

    fmt.Printf("Enter rating [0-5]: ")
    fmt.Scanln(&score)
    if score == "" {
        qry.Add("rating", "0")
    } else {
        i, _ := strconv.ParseFloat(score, 32)
        score = fmt.Sprintf("%d", int(i*2))
        qry.Add("rating", score)
    }
    fmt.Printf("(optional) Enter review: ")
    review, _ = input.ReadString('\n')
    qry.Add("review", review)
    req.URL.RawQuery = qry.Encode()

    client.Do(req)
    fmt.Println("Submitted") // Check JSON to ensure success
}

func main() {
    if len(os.Args) != 3 {
        fmt.Printf("Incomplete command\n\n")
        help()
    }
    user, set := os.LookupEnv("boxu")
    if !set {
        need_env()
    }
    pass, set := os.LookupEnv("boxp")
    if !set {
        need_env()
    }

    client, csrf := login(user, pass)
    switch os.Args[1] {
    case "like":
        iLike(os.Args[2], client, csrf)
    case "review":
        review(os.Args[2], client, csrf)
    case "search":
        search(os.Args[2], client)
    default:
        fmt.Printf("Available subcommands: `like', `review', `search'\n\n")
        help()
    }
}

